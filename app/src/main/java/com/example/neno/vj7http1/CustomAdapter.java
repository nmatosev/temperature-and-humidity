package com.example.neno.vj7http1;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.net.ContentHandler;
import java.util.List;

/**
 * Created by neno on 10.11.2015..
 */
public class CustomAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    public CustomAdapter(Context context)
    {

        mContext = context;
        mInflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
       return DataStorage.cars.length;
    }

    @Override
    public Object getItem(int position) {
        //return carItem.get(position);
        return  null;


    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = mInflater.inflate(R.layout.item,parent,false);

        Log.d("SCROLL", "view number " + position);

        //ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnailImg);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        TextView temp = (TextView) convertView.findViewById(R.id.temp);
        TextView hum = (TextView) convertView.findViewById(R.id.hum);



            time.setText((DataStorage.cars[position].time).toString());
            temp.setText("  Temperature " + (DataStorage.cars[position].temperature).toString()+ "°C"); //podatak obavezno u string inače puca APP
            //Log.d("aaaaaaaaa", "manufacturer " + DataStorage.cars[position].manufacturer);//pozicija je int postavljen na 0,ink skrolon
            hum.setText("  Humidity " + (DataStorage.cars[position].humidity).toString()+ "%"); //pristupanje datstorageu na poziciju objekta

            //UrlImageViewHelper.setUrlDrawable(thumbnail,DataStorage.cars[position].thumbnail);



        return convertView;
    }



}
