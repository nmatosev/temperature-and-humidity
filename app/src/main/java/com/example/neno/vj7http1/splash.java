package com.example.neno.vj7http1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * Created by neno on 12.11.2015..
 */
public class splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        final ImageView imageView = (ImageView) findViewById(R.id.imageView);
        final Animation animation =AnimationUtils.loadAnimation(getBaseContext(),R.anim.rotate);

        final Animation animation2 =AnimationUtils.loadAnimation(getBaseContext(),R.anim.abc_fade_out);


        imageView.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
//a
            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.startAnimation(animation2);
                Log.d("gotova anim","end");
                finish();
                Intent i = new Intent(getBaseContext(), Login.class);
                startActivity(i);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }





    @Override
    protected void onPause() {
        super.onPause();
    }
}
