package com.example.neno.vj7http1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
//import com.github.nkzawa.emitter.Emitter;
//import com.github.nkzawa.socketio.client.IO;
//import com.github.nkzawa.socketio.client.Socket;

import static android.app.PendingIntent.getActivity;


/**
 * Created by neno on 22.11.2015..
 */
public class Login extends Activity {

    private static final String TAG_USERNAME = "username";
    private static final String TAG_PASS = "pass";
    private static final String TAG_SUCCESS = "success";
  //  private static final String TAG_MESSAGE = "message";
    private String URL = "http://10.0.3.2:3000/contact";   //EMULATOR
    //private static final String url_update = "http://10.0.3.2:8080/login.php";//emulator
   // private static final String url_update = "http://192.168.1.8:8080/login.php";



    JSONParser jsonParser = new JSONParser();

    private ProgressDialog pDialog;

    EditText username;
    EditText password;
    TextView tv;
    TextView getinput;
    TextView tv2;
    String user;
    String pass;
    Button login;

    private Socket socket;
    {
        try {
            socket = IO.socket("http://10.0.3.2:4000");  //konekcija na server
        } catch (URISyntaxException e) {}
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        getinput = (TextView) findViewById(R.id.getinput);

        Button login = (Button) findViewById(R.id.login);

        socket.connect();   //spoji se na server
        //Starts listening for server-sent events pod string tagom
        socket.on("tag_success", handleIncomingMessages);  //hendlaj dobivene poruke



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verify();
                //new SaveProductDetails().execute();
            }
        });


    }

    private void verify(){
        String user = username.getText().toString();
        String pass = password.getText().toString();

        //tv = (TextView) findViewById(R.id.);
        //String message = username.getText().toString();
        socket.emit("message",user,pass);
      //  socket.emit("message1",pass);

    }


    private Emitter.Listener  handleIncomingMessages = new Emitter.Listener() {  //osluškuj sa servera
        @Override
        public void call(final Object... args) {
            Login.this.runOnUiThread(new Runnable() { //ako fragment onda get activity
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];  //primi podatak sa strane servera u jsonu
                    String message;
                    try {

                        message = data.getString("tag_success").toString();  //primi podatak sa servera pod tagom message

                        int success = data.getInt("tag_success");
                        Log.d("primljeno", data.toString());
                        Log.d("primljeno", message);
                        if(success==1) {
                            Intent intent = new Intent(getApplicationContext(), Pocetna.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Uspješan login!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Krivi podaci za login!", Toast.LENGTH_SHORT).show();
                        }

                    }
                    catch (JSONException e) {

                    }
                }


            });

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }

    class SaveProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Pokušaj logina ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            String user = username.getText().toString();
            String pass = password.getText().toString();
            String loginData = user;
            //String message = username.getText().toString();
            socket.emit("loginData",loginData);
            return(loginData);
           /* String user = username.getText().toString();
            String pass = password.getText().toString();
            //vrijednosti ne smiju biti null
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_USERNAME,user));
            params.add(new BasicNameValuePair(TAG_PASS,pass));


            // check json success tag
            JSONObject json = jsonParser.makeHttpRequest(URL,
                    "POST", params);

            // check json success tag
            try {
                Log.d("try", "Pokušaj pristupa bazi");
                int success = json.getInt(TAG_SUCCESS);
                Log.d("resp", json.getString(TAG_SUCCESS));
                if (success == 1) {

                    Log.d("response", "Uspješan login"); // response PHP skripte

                    Intent intent = new Intent(getApplicationContext(),Pocetna.class);
                    finish();
                    startActivity(intent);

                    return json.getString(TAG_MESSAGE);

                } else {

                    Log.d("response", "Krivi podaci za login!");
                    return json.getString(TAG_MESSAGE);////message varijabla iz php skripte

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;*/


        }

        protected void onPostExecute(String message) {

            pDialog.dismiss();
            if (message != null){
                Toast.makeText(Login.this, "Dobrodošao " + message, Toast.LENGTH_LONG).show(); //message varijablu pretvori u toast
            }
        }






    }
}
