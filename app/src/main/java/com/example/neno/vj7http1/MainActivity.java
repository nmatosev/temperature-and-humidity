package com.example.neno.vj7http1;

import android.app.Activity;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.http.Header;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;

import org.json.JSONArray;

import java.net.URL;


public class MainActivity extends Activity{

    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private Gson mGson = new Gson();
    private ListView listView;


    private CustomAdapter adapter;
    //private String URL = "http://profi.co/vjezbe/android/cars.txt";
    private String URL = "http://10.0.3.2:4000/contact";   //EMULATOR
   // private String URL = "http://192.168.1.3:3000/contact"; //MOBITEL

    private TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);//kreiraj novi obj tipa lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(),CarDetails.class);
                intent.putExtra("position",position); //proslijedi position u drugi activity
                startActivity(intent);


            }


        });

        httpClient.get(URL, new JsonHttpResponseHandler() { //get zahtjev,dobavi JSON sa URL
            @Override
            public void onSuccess(JSONArray response) {
                DataStorage.cars = mGson.fromJson(response.toString(),Car[].class);  //JSON konvertaj u string, napravi Car objekte
                Log.d("RESPONSE", "uspjeh");

                adapter = new CustomAdapter(getApplicationContext());//adapter u trenutnom aktivitiju

                listView.setAdapter(adapter);//adapter stavi u listview

            }

            @Override
            public void onFailure(Throwable error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }


        });



    }



}


