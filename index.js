
var express=require('express');//TEMPHUM
var app=express();
//var app = require('express')();  //
var http = require ('http').Server(app);/////UKLJUČIVANJE NODE MODULA KOJI ĆE SE KORISTITI
var io = require('socket.io')(http);
var mongojs = require("mongojs");
var db=mongojs('temphum',['temphum'],['user']);//koja baza,koja kolekcija
var bodyParser=require('body-parser');//parsiraj podatke iz body

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/',function(req,res){  //na lokaciju /
	res.sendFile(__dirname+'public/index.html');  //postavi index.html
})

io.on('connection', function(socket){  //OSLUŠKIVANJE
	console.log('one user connected'+socket.id);  //stvori socket na strani servera,detektiraj konekciju
	
	socket.on("message", function(data1,data2){  //tag pod kojim se nalazi string
		console.log(data1,data2);  //logaj podatak sa android aplikacije
	
		var sockets = io.sockets.sockets;
			sockets.forEach(function(sock){
				
													//arg i callback funkcija kao argument
					 //pristupi objektu koji ima username data	
						 
						
								db.collection('user').findOne( {username:data1,password:data2} ,function(err,docs){
									if(docs!=null)  //pokušaj pronaci objekt sa tim usernameom i passom,ako postoji,klijentu vrati signal 1
											{
											sock.emit('tag_success',{tag_success:"1"})  ////prosljedi klijentu tagsuccess 1
											}
										else
											{
											sock.emit('tag_success',{tag_success:"0"})  //prosljedi klijentu tagsuccess 0
											}
											
									console.log(docs);
								});
																					
			})
		
	})
	socket.on('disconnect', function(){
		console.log('one user has disconnected'+ socket.id);
	})
})

////DOBAVI IZ BAZE 
app.get('/contact', function(req,res)//lokacija
	{			
			console.log("I RECEIVED GET REQUEST-server")			
			db.temphum.find(function(err,docs)//dobavi podatke iz baze
				{
					console.log(docs);//logaj dobavljeno					
					res.json(docs);	//slanje klijentu u json formatu	
				});
				
	
	});
	
	
	app.get('/user', function(req,res)//lokacija
	{			
			console.log("I RECEIVED GET REQUEST-server")			
			db.collection('user').find(function(err,docs)//dobavi podatke iz baze
				{
					console.log(docs);//print fetched					
					res.json(docs);	//slanje  u kontroler/klijent u json formatu	
				});
				
				io.on('connection', function(socket){
					console.log('one user connected'+socket.id);  //stvori socket na strani servera,detektiraj konekciju
					
					socket.on("loginData", function(data){  //tag pod kojim se nalazi string
						console.log(data);  //logaj podatak sa android aplikacije						
					
					
					})
					
					
				})
					
	});
	

	
////POSTAVI NOVO U BAZU
app.post('/contactlist', function (req, res) {
  console.log(req.body);//zatraži od klijenta objekt,zapisi ga u konzoli
  db.temphum.insert(req.body, function(err, doc) {//zatraži iz body-a objekt i upiši ih u db
    res.json(doc);//posalji objekt nazad klijentu
  });
});

////BRIŠI
app.delete('/contactlist/:id', function (req, res) {//briši item
  var id = req.params.id;  //u varijablu id spremi id parametra(svaki objekt definiran sa 4 parametra)
  console.log(id);  //logaj id

   db.temphum.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {  //pristupi bazi i izbriši objekt koji ima id iz varijable id
    res.json(doc);//slanje null vr. nazad klijentu
  });
});

////DOBAVI IZ BAZE ZA IZMJENU
app.get('/contactlist/:id', function (req, res) {//uzmi id sa klijenta
  var id = req.params.id;//u varijablu id spremi id uzet sa klijenta
  console.log(id);  //logaj id u konzolu
  db.temphum.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {  //pronađi u bazi objekt s id-em preuzetim od klijenta
    res.json(doc);   //pošalji objekt klijentu
  });
});


////POSTAVI IZMJENJENO U BAZU
app.put('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.time);  //logaj time na kozolu servera
  db.temphum.findAndModify({	//pristupi i izmjeni bazi temphum
    query: {_id: mongojs.ObjectId(id)},  //gdje je u bazi navedeni id
    update: {$set: {time: req.body.time, temperature: req.body.temperature, humidity: req.body.humidity}}, //izmjeni
    new: true}, function (err, doc) {
      res.json(doc); //vrati klijentu novi objekt
    }
  );
});


http.listen(4000,function(){
console.log('server running on 4000');
});